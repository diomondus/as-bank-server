package com.butilov.asbankserver.service;

import com.butilov.asbankserver.entity.BankUser;
import com.butilov.asbankserver.entity.Bill;
import com.butilov.asbankserver.entity.Credit;
import com.butilov.asbankserver.entity.Transaction;
import com.butilov.asbankserver.repository.BankUsersRepository;
import com.butilov.asbankserver.repository.CreditsRepository;
import org.springframework.stereotype.Service;

import java.util.Calendar;

/**
 * Created by Dmitry Butilov
 * on 12.06.18.
 */
@Service
public class CreditService {
    private static final String CREDITOR = "$BANK_MONEY$";

    private CreditsRepository creditsRepository;
    private BankUsersRepository bankUsersRepository;

    private BillService billService;
    private TransactionService transactionService;

    public CreditService(CreditsRepository creditsRepository,
                         BankUsersRepository bankUsersRepository,
                         BillService billService,
                         TransactionService transactionService) {
        this.creditsRepository = creditsRepository;
        this.bankUsersRepository = bankUsersRepository;
        this.billService = billService;
        this.transactionService = transactionService;
    }

    public void saveCredit(Credit credit) {
        creditsRepository.save(credit);
    }

    public void createCreditAndSave(Credit credit, BankUser bankUser) {
        String billName = bankUser.getLogin() + " credit" + (bankUser.getCredits().size());
        Bill creditBill = billService.createBillWithName(billName);
        credit.setBill(creditBill);
        credit.setExpireTime(getTimeForOneYearDebet());
        credit.setUser(bankUser);
        creditsRepository.save(credit);

        Transaction transaction = new Transaction();
        transaction.setUser(getCreditor());
        transaction.setDestBill(bankUser.getBill());
        transaction.setValue(credit.getValue());
        transactionService.saveTransaction(transaction);

        bankUser.getCredits().add(credit);
        bankUsersRepository.save(bankUser);
    }

    private long getTimeForOneYearDebet() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.YEAR, 1);
        return calendar.getTimeInMillis();
    }

    private BankUser getCreditor() {
        return bankUsersRepository.findByLogin(CREDITOR);
    }
}