package com.butilov.asbankserver.controller;

import com.butilov.asbankserver.entity.BankUser;
import com.butilov.asbankserver.entity.Bill;
import com.butilov.asbankserver.entity.Transaction;
import com.butilov.asbankserver.service.TransactionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping(path = "/", produces = MediaType.APPLICATION_JSON_VALUE)
public class TransactionController {

    private TransactionService transactionService;

    public TransactionController(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    @PostMapping(path = "transaction")
    @ResponseBody
    public void transaction(@RequestBody Transaction transaction) {
        transactionService.saveTransaction(transaction);
    }

    // todo тестовый метод
    @GetMapping(path = "transaction")
    public Transaction getTransaction() {
        Transaction transaction = new Transaction();
        transaction.setValue(1000);
        transaction.setTime(System.nanoTime());
        transaction.setUser(new BankUser() {{
            setLogin("user1");
            setPassword("1");
        }});
        transaction.setDestBill(new Bill() {{
            setName("user2's Bill");
        }});
        return transaction;
    }
}