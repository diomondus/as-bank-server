package com.butilov.asbankserver.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Collection;

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Bill {

    @Id
    @Column
    @JsonIgnore
    @GeneratedValue
    private Long id;

    @NotNull
    @Column(length = 100)
    private String name;

    @NotNull
    private long value;

    @JsonIgnore
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Collection<Transaction> transactions;
}